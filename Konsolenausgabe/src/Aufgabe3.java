
public class Aufgabe3 {

	public static void main(String[] args) {
		
		double f1,f2,f3,f4,f5,d1,d2,d3,d4,d5;
		
		f1 = -20;
		f2 = -10;
		f3 = 0;
		f4 = 20;
		f5 = 30;
		
		d1 = (f1 - 32) * 5/9;
		d2 = (f2 - 32) * 5/9;
		d3 = (f3 - 32) * 5/9;
		d4 = (f4 - 32) * 5/9;
		d5 = (f5 - 32) * 5/9;
		
		System.out.printf("Fahrenheit | Celsius%n"); 
		System.out.printf("--------------------%n");
		System.out.printf("%-11s|%8.6s%n", f1,d1); 
		System.out.printf("%-11s|%8.6s%n", f2,d2);
		System.out.printf("+%-10s|%8.6s%n", f3,d3);
		System.out.printf("+%-10s|%8.6s%n", f4,d4);
		System.out.printf("+%-10s|%8.6s%n", f5,d5);
	}

}
