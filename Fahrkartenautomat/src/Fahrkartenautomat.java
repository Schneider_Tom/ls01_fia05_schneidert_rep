﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int ticketzahl = 0;
       int ticket = 0;
       

       //System.out.print("Zu zahlender Betrag (EURO): ");
       //zuZahlenderBetrag = tastatur.nextDouble();
       
       while(true) {
	       
       	while(ticket == 0) {
	       System.out.printf("Für eine Kurzstecke drücken sie die: 1%nFür eine Langstecke drücken sie die: 2%nFür eine Tageskarte drücken sie die: 3%n");
	       ticket = tastatur.nextInt();
	       }
	       
	       while(ticket >= 5) {
	           System.out.printf("Für eine Kurzstecke drücken sie die: 1%nFür eine Langstecke drücken sie die: 2%nFür eine Tageskarte drücken sie die: 3%n");
	           ticket = tastatur.nextInt();
	           }
	       
	       //zuZahlenderBetrag Einfügen
	       while(ticket == 1) {
	    	   zuZahlenderBetrag = 1;
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt(); 
	           }
	
	           // Geldeinwurf
	           // -----------
	    	   zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
	    	   eingezahlterGesamtbetrag = 0.0;
	           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	           {
	        	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	        	   System.out.printf("Anzahl der Tickets: %s\n", ticketzahl);
	        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	        	   eingeworfeneMünze = tastatur.nextDouble();
	               eingezahlterGesamtbetrag += eingeworfeneMünze;
	           }
	
	           // Fahrscheinausgabe
	           // -----------------
	           System.out.println("\nFahrschein wird ausgegeben");
	           for (int i = 0; i < 8; i++)
	           {
	              System.out.print("=");
	              try {
	    			Thread.sleep(250);
	    		} catch (InterruptedException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	           }
	           System.out.println("\n\n");
	
	           // Rückgeldberechnung und -Ausgabe
	           // -------------------------------
	           rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	           if(rückgabebetrag > 0.0)
	           {
	        	   System.out.printf("%s %.2f %s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
	        	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	
	               while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	               {
	            	  System.out.println("2 EURO");
	    	          rückgabebetrag -= 2.0;
	               }
	               while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	               {
	            	  System.out.println("1 EURO");
	    	          rückgabebetrag -= 1.0;
	               }
	               while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	               {
	            	  System.out.println("50 CENT");
	    	          rückgabebetrag -= 0.5;
	               }
	               while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	               {
	            	  System.out.println("20 CENT");
	     	          rückgabebetrag -= 0.2;
	               }
	               while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	               {
	            	  System.out.println("10 CENT");
	    	          rückgabebetrag -= 0.1;
	               }
	               while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	               while(rückgabebetrag == 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	           }
	
	            if (ticketzahl == 1) {
	            	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                        "vor Fahrtantritt entwerten zu lassen!\n"+
	                        "Wir wünschen Ihnen eine gute Fahrt mit der DB.");}
	            	else {
	            		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                            "vor Fahrtantritt entwerten zu lassen!\n"+
	                            "Wir wünschen Ihnen eine gute Fahrt mit der DB.");
	            	}
	            Fahrkartenautomat.timea();
	            System.out.printf("%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
	            ticketzahl = 0;
	            ticket = 6;
	       }
	       while(ticket == 2) {
	    	   zuZahlenderBetrag = 2;
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt(); 
	           }
	
	           // Geldeinwurf
	           // -----------
	    	   zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
	    	   eingezahlterGesamtbetrag = 0.0;
	           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	           {
	        	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	        	   System.out.printf("Anzahl der Tickets: %s\n", ticketzahl);
	        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	        	   eingeworfeneMünze = tastatur.nextDouble();
	               eingezahlterGesamtbetrag += eingeworfeneMünze;
	           }
	
	           // Fahrscheinausgabe
	           // -----------------
	           System.out.println("\nFahrschein wird ausgegeben");
	           for (int i = 0; i < 8; i++)
	           {
	              System.out.print("=");
	              try {
	    			Thread.sleep(250);
	    		} catch (InterruptedException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	           }
	           System.out.println("\n\n");
	
	           // Rückgeldberechnung und -Ausgabe
	           // -------------------------------
	           rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	           if(rückgabebetrag > 0.0)
	           {
	        	   System.out.printf("%s %.2f %s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
	        	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	
	               while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	               {
	            	  System.out.println("2 EURO");
	    	          rückgabebetrag -= 2.0;
	               }
	               while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	               {
	            	  System.out.println("1 EURO");
	    	          rückgabebetrag -= 1.0;
	               }
	               while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	               {
	            	  System.out.println("50 CENT");
	    	          rückgabebetrag -= 0.5;
	               }
	               while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	               {
	            	  System.out.println("20 CENT");
	     	          rückgabebetrag -= 0.2;
	               }
	               while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	               {
	            	  System.out.println("10 CENT");
	    	          rückgabebetrag -= 0.1;
	               }
	               while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	               while(rückgabebetrag == 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	           }
	
	            if (ticketzahl == 1) {
	            	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                        "vor Fahrtantritt entwerten zu lassen!\n"+
	                        "Wir wünschen Ihnen eine gute Fahrt mit der DB.");}
	            	else {
	            		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                            "vor Fahrtantritt entwerten zu lassen!\n"+
	                            "Wir wünschen Ihnen eine gute Fahrt mit der DB.");
	            	}
	            Fahrkartenautomat.timea();
	            System.out.printf("%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
	            ticketzahl = 0;
	            ticket = 6;
	       }
	       while(ticket == 3) {
	    	   zuZahlenderBetrag = 5;
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt(); 
	           }
	
	           // Geldeinwurf
	           // -----------
	    	   zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
	    	   eingezahlterGesamtbetrag = 0.0;
	           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	           {
	        	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
	        	   System.out.printf("Anzahl der Tickets: %s\n", ticketzahl);
	        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	        	   eingeworfeneMünze = tastatur.nextDouble();
	               eingezahlterGesamtbetrag += eingeworfeneMünze;
	           }
	
	           // Fahrscheinausgabe
	           // -----------------
	           System.out.println("\nFahrschein wird ausgegeben");
	           for (int i = 0; i < 8; i++)
	           {
	              System.out.print("=");
	              try {
	    			Thread.sleep(250);
	    		} catch (InterruptedException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	           }
	           System.out.println("\n\n");
	
	           // Rückgeldberechnung und -Ausgabe
	           // -------------------------------
	           rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	           if(rückgabebetrag > 0.0)
	           {
	        	   System.out.printf("%s %.2f %s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
	        	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	
	               while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	               {
	            	  System.out.println("2 EURO");
	    	          rückgabebetrag -= 2.0;
	               }
	               while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	               {
	            	  System.out.println("1 EURO");
	    	          rückgabebetrag -= 1.0;
	               }
	               while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	               {
	            	  System.out.println("50 CENT");
	    	          rückgabebetrag -= 0.5;
	               }
	               while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	               {
	            	  System.out.println("20 CENT");
	     	          rückgabebetrag -= 0.2;
	               }
	               while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	               {
	            	  System.out.println("10 CENT");
	    	          rückgabebetrag -= 0.1;
	               }
	               while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	               while(rückgabebetrag == 0.05)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	           }
	
	            if (ticketzahl == 1) {
	            	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                        "vor Fahrtantritt entwerten zu lassen!\n"+
	                        "Wir wünschen Ihnen eine gute Fahrt mit der DB.");}
	            	else {
	            		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                            "vor Fahrtantritt entwerten zu lassen!\n"+
	                            "Wir wünschen Ihnen eine gute Fahrt mit der DB.");
	            	}
	            Fahrkartenautomat.timea();
	            System.out.printf("%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
	            ticketzahl = 0;
	            ticket = 6;
	       }
	       
	       
	        
	    
	      
	    
	    } 
       
    }
    public static void timea() {
    	try {
    		Thread.sleep(5000);
    	}
    	catch(Exception e) {
    		
    	}
    }

}