import java.util.Scanner;

class Fahrkartenautomat2
{
	static Scanner tastatur = new Scanner(System.in);
    
	static double zuZahlenderBetrag = 0; 
	static double eingezahlterGesamtbetrag;
	static double eingeworfeneM�nze;
	static double r�ckgabebetrag;
	static int ticketzahl = 0;
	static int ticket = 0;
	static int zugeld = 0;
	
	public static void main(String[] args){//Hier werden die Methoden aufgerufen      
       while(true) {//Code wird immer Durchgef�hrt 
    	   fahrkartenbestellungErfassen();
    	   fahrkartenBezahlen();
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben();
    	   reset();
	 }}
       
	public static void fahrkartenbestellungErfassen(){//Hier wird das Ticket gew�hlt
		while(ticket == 0) {//Starte Ticket abfrage
		       System.out.printf("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)%nTageskarte Regeltarif AB [8,60 EUR] (2)%nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)%n");
		       ticket = tastatur.nextInt();
		       }
		while(ticket >= 4) {//Falsche Eingaben
		       System.out.printf(">>falsche Eingabe<<%n");
		       ticket = tastatur.nextInt();
		       }
		while(ticket == 1) {//Ticket 1
	    	   zuZahlenderBetrag = 2.90;
	    	   System.out.printf("Ihre Wahl: %s\n", ticket);
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt(); 
	               zugeld = 1;
	    	   }}
		 while(ticket == 2) {//Ticket 2
	    	   zuZahlenderBetrag = 8.60;
	    	   System.out.printf("Ihre Wahl: %s\n", ticket);
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt(); 
	               zugeld = 1;
	           }}
		 while(ticket == 3) {//Ticket 3
	    	   zuZahlenderBetrag = 23.50;
	    	   System.out.printf("Ihre Wahl: %s\n", ticket);
	    	   ticket = 5;
	    	   while(ticketzahl == 0) {
	        	   System.out.print("Anzahl der Tickets:");
	               ticketzahl = tastatur.nextInt();
	               zugeld = 1;
	           }}
	}
	public static void fahrkartenBezahlen(){//Geldeinwurf
 	   zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
 	   eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "Euro");
     	   System.out.printf("Anzahl der Tickets: %s\n", ticketzahl);
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
	}
	
	public static void fahrkartenAusgeben(){// Fahrscheinausgabe
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(){// R�ckgeldberechnung und -Ausgabe
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("%s %.2f %s\n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag == 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
		
	}
	
	public static void reset(){//Abschlusssatz+Reset von allen Variablen so dass der code erneut angefangen werden kann
		if (ticketzahl == 1) {//Abschlusssatz
        	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir w�nschen Ihnen eine gute Fahrt mit der DB.");}
        	else {
        		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                        "vor Fahrtantritt entwerten zu lassen!\n"+
                        "Wir w�nschen Ihnen eine gute Fahrt mit der DB.");
        	}
        Fahrkartenautomat.timea();//Reset von allen Variablen so dass der code erneut angefangen werden kann
        System.out.printf("%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
        ticketzahl = 0;
        ticket = 0;
	}
	
    public static void timea() {//Sleeptimer von 5000
    	try {
    		Thread.sleep(5000);
    	}
    	catch(Exception e) {
    		
    	}
    }

}